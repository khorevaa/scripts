@echo off
rem ---------------------------------------------------
rem ��⮬���᪠� ���㧪� ९������ 1� � git
rem �ᯮ�짮�����
rem repo_git.bat --path_1c path --mode init --ib_path path --repo_path path --repo_user user --repo_pass password [--repo_base path]
rem 
rem
rem ShmalevoZ
rem  2017-05
rem ---------------------------------------------------

setlocal enabledelayedexpansion

rem ��� errorlevel ������
set ERROR_MASSAGE=
set ERROR_CODE=0

rem ��騥 ��६����
set RESULT=
set DEBUG=1

rem ���� � ��㬥��� ��-� ����
:loop_parse_args_not_empty
if '%1'=='' goto loop_parse_args_end

	rem �맮� ����ணࠬ�� �஢�ન �� ����稥 ����/���祭��
	call :parse_args %1 %2
	rem ���饭�� �� ᫥���騩 ��㬥��
	shift
	rem �த������� 横��
	goto :loop_parse_args_not_empty
:loop_parse_args_end

rem ����室��� ��६����
set "EXEC_BIN=!path_1c!\bin\1cv8.exe"
call :get_1c_repo_params "!repo_path!" "!repo_user!" "!repo_pass!"
set "REPO_ARGS=!RESULT!"
if not DEBUG==0 (
	echo EXEC_BIN==!EXEC_BIN!
	echo REPO_ARGS==!REPO_ARGS!
)

rem �஢��塞 ��㬥���
call :verify_args
if not !ERROR_CODE!==0 (
	goto :END
)

rem �롮� ०���
if "!mode!"=="init" (
	call :mode_init "!ib_path!" "!repo_base!" "!REPO_ARGS!"
)

rem ����砭�� �ணࠬ��
:END
if not !ERROR_CODE!==0 (
	echo !ERROR_MESSAGE!
)
endlocal
exit !ERROR_CODE!

rem ---------------------------------------------------
rem ���樠������� git �࠭���� � ��⠫��� �࠭���� 1�
rem � ࠧ���稢��� ���� ������ ������祭��� � �⮬� �࠭�����
rem %1 ��⠫�� ��
rem %2 ��⠫�� �࠭���� 1�
rem %3 ��ࠬ���� ������祭�� � �࠭�����
:mode_init

	set "_IB_PATH=%~1"
	set "_REPO_BASE=%~2"
	set "_REPO_ARGS=%~3"

	rem ���樠�����㥬 �����쭮� git �࠭���� � ��⠫��� �࠭���� 1�
	if not !DEBUG!==0 echo ���樠������ git ९������ � ��⠫��� !_REPO_BASE!
	if exist "!_REPO_BASE!\.git" (
		echo Git ९����਩ � ��⠫��� �࠭���� 1� 㦥 �������
	) else (
		cd "!_REPO_BASE!"
		git init
		git config user.email "auto@git.localhost"
		git config user.name "auto"
		git add *
		git commit -m "Initial"
	)

	rem �������� ���ଠ樮���� ����
	"!EXEC_BIN!" Config CreateInfoBase /F "!_IB_PATH!" /Locale ru_RU /DisableStartupMessages !_REPO_ARGS! /ConfigurationRepositoryUpdateCfg /UpdateDBCfg /AddInList "!_IB_PATH!" -force

exit /b
rem mode_init
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� ��ࠬ���� ������祭�� � �࠭����� ����᪠ 1� 
rem %1 ���� ������祭�� � �࠭����� 1�
rem %2 ���짮��⥫�
rem %3 �����
:get_1c_repo_params
	
	set RESULT=/ConfigurationRepositoryF "%~1" /ConfigurationRepositoryN "%~2" /ConfigurationRepositoryP "%~3"

exit /b
rem get_1c_repo_params
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �஢�ઠ ��㬥�⮢ ��������� ��ப�
:verify_args

	rem �ᯮ��塞� 䠩� 1�
	if not exist "!EXEC_BIN!" (
		set "ERROR_MESSAGE=�� 㤠���� ���� �ᯮ��塞� 䠩� 1� !EXEC_BIN!. ����୮ 㪠��� ��ࠬ��� --path_1c"
		set ERROR_CODE=1
	)
	rem ���४⭮��� ��������� ०���
	if not '!mode!'=='init' (
		set "ERROR_MESSAGE=����୮� ���祭�� ��ࠬ��� mode"
		set ERROR_CODE=2
	)
	rem ���४⭮��� ��ࠬ��஢ ��� ०��� init
	rem --base_path path --repo_path path --repo_user user --repo_pass password [--repo_base path]
	if '!mode!'=='init' (
		if not exist "!ib_path!" (
			set "ERROR_MESSAGE=�� ������ ��⠫�� ���� ������ 1� '!ib_path!'. ������ ��ࠬ��� ib_path"
			set ERROR_CODE=3
		)
		if exist "!ib_path!\1Cv8.1CD" (
			set "ERROR_MESSAGE=� ��⠫��� ���ଠ樮���� ���� '!ib_path!' ������� ��������� ���ଠ樮���� ����. ������ ��ࠬ��� ib_path"
			set ERROR_CODE=4
		)
		if not exist "!repo_base!" (
			set "ERROR_MESSAGE=�� ������ ��⠫�� �࠭���� 1� '!repo_base!'. ������ ��ࠬ��� repo_base"
			set ERROR_CODE=5
		)
	)
	rem ����稥 git
	call :can_run_command "git.exe"
	if not !RESULT!==1 (
		set "ERROR_MESSAGE=�� 㤠���� ���� git.exe"
		set ERROR_CODE=3
	)

exit /b
rem verify_args
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �஢�ઠ ���������� �믮������ �������
rem %1 ��� �ᯮ��塞��� 䠩��
:can_run_command
	
	if not !DEBUG!==0 echo �஢�ઠ ���������� �믮������ %1
	set RESULT=1
	set "temp_full_path=%~$PATH:1"
	if not !DEBUG!==0 echo ����� ���� '!temp_full_path!'
	if '!temp_full_path!'=='' (
		set RESULT=0
	)
	if not !DEBUG!==0 echo �����頥� !RESULT!

exit /b
rem can_run_command
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �८�ࠧ������ ��ࠬ��஢ ��������� ��ப�
rem � ��६���� �।�
rem %1 ���� ���� --<key>
rem %2 ���祭��
:parse_args

	rem �஢�ઠ �� ����稥 ����室����� ��䨪� � ����
	set "prefix=%~1"
	set "prefix=%prefix:~0,2%"

	if "!prefix!"=="--" (
		rem ��䨪� ᮮ⢥����� ����室�����
		for /f "tokens=1* delims=--" %%a in ("%~1") do (
			set "%%a=%~2"
			rem �⫠��筮� ᮮ�饭��
			if not !DEBUG!==0 echo key is %%a value is %2
		)
	)

exit /b
rem parse_args
rem ---------------------------------------------------

